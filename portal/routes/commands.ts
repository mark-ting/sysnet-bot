import { Router } from 'express'
import { Core } from 'src/Core'

export const commands = (core: Core) => {
  const router = Router()

  router.get('/', (req, res) => {
    res.render('commands/commandlist', {
      title: `All Commands`,
      commandList: [...core.commandList.values()]
    })
  })

  router.get('/:commandName', (req, res) => {
    let commandName = req.params.commandName
    res.render('commands/command', {
      title: `Command: ${commandName}`,
      command: core.commandList.get(commandName)
    })
  })

  return router
}
