import { Router } from 'express'
import { FGOProfile, profileFromSqlRow } from 'modules/fgo/interfaces/FGOProfile'
import { IDatabase } from 'pg-promise'
import { Core } from 'src/Core'

export const api = (core: Core) => {
  const router = Router()
  const db = core.store.getDb('fgo') as IDatabase<{}>

  router.get('/fgo/profiles', async (req, res) => {
    let profiles: FGOProfile[] = []

    const query = {
      text: 'SELECT * FROM fgo.jp_profile'
    }
    try {
      const data = await db.manyOrNone(query)
      profiles = data.map(profileFromSqlRow)
      if (profiles.length < 1) {
        res.status(404).end()
      } else {
        res.status(200).json(profiles)
      }
    } catch (err) {
      core.logger.error(err)
      res.status(500).end()
    }
  })

  router.get('/fgo/profile/:discordId', async (req, res) => {
    const discordId = req.params.discordId

    const query = {
      text: 'SELECT * FROM fgo.jp_profile WHERE discord_id = $1',
      values: [discordId]
    }
    try {
      const data = await db.oneOrNone(query)
      const profile = profileFromSqlRow(data)
      if (!profile) {
        res.status(404).end()
      } else {
        res.status(200).json(profile)
      }
    } catch (err) {
      core.logger.error(err)
      res.status(500).end()
    }
  })

  router.all('*', (req, res) => {
    res.status(400).end()
  })

  return router
}
