import { Router } from 'express'
import { api } from 'portal/routes/api'
import { commands } from 'portal/routes/commands'
import { Core } from 'src/Core'
import { fgojp } from './fgojp'
import { fgona } from './fgona'

export const index = (core: Core) => {
  const router = Router()

  router.get('/', (req, res) => {
    res.render('index', {
      title: 'Home',
      commandList: core.commandList,
      taskList: core.taskList
    })
  })

  router.use('/commands', commands(core))
  // router.use('/tasks', tasks(core))
  router.use('/fgojp', fgojp(core))
  router.use('/fgona', fgona(core))
  router.use('/api', api(core))

  return router
}
