import { Router } from 'express'
import { Core } from 'src/Core'

export const tasks = (core: Core) => {
  const router = Router()

  router.get('/', (req, res) => {
    res.render('tasks/tasklist', {
      title: `All Tasks`,
      taskList: [...core.taskList.values()]
    })
  })

  router.get('/:taskName', (req, res) => {
    let taskName = req.params.taskName
    res.render('tasks/task', {
      title: `Task: ${taskName}`,
      task: core.taskList.get(taskName)
    })
  })

  return router
}
