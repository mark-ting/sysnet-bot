import { Router } from 'express'
import { FGOProfile, profileFromSqlRow } from 'modules/fgo/interfaces/FGOProfile'
import { IDatabase } from 'pg-promise'
import { Core } from 'src/Core'

const region = 'jp'

export const fgojp = (core: Core) => {
  const router = Router()
  const db = core.store.getDb('fgo') as IDatabase<{}>

  router.get('/', async (req, res) => {
    let profiles: FGOProfile[] = []

    const query = {
      text: 'SELECT * FROM fgo.jp_profile'
    }
    try {
      const data = await db.manyOrNone(query)
      profiles = data.map(profileFromSqlRow)
      res.render('fgo/profilelist', {
        title: 'F/GO JP Profiles',
        region: region,
        profiles: profiles
      })
    } catch (err) {
      core.logger.error(err)
    }
  })

  router.get('/profile/:discordId', async (req, res) => {
    const discordId = req.params.discordId

    const query = {
      text: 'SELECT * FROM fgo.jp_profile WHERE discord_id = $1',
      values: [discordId]
    }
    try {
      const data = await db.oneOrNone(query)
      const profile = profileFromSqlRow(data)
      const user = await core.client.fetchUser(profile.discordId)
      const username = `${user.username}#${user.discriminator}`
      res.render('fgo/profile', {
        title: `F/GO JP Profile for ${profile.name}`,
        profile: profile,
        username: username
      })
    } catch (err) {
      core.logger.error(err)
    }
  })

  return router
}
