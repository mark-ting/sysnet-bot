const gulp = require('gulp')
const del = require('del')
const path = require('path')

gulp.task('clean', () => {
  return del([
    path.join(__dirname, 'main.js'),
    path.join(__dirname, 'main.js.map'),
    path.join(__dirname, 'src', '**', '*.js'),
    path.join(__dirname, 'src', '**', '*.js.map'),
    path.join(__dirname, 'modules', '**', '*.js'),
    path.join(__dirname, 'modules', '**', '*.js.map'),
    path.join(__dirname, 'portal', '**', '*.js'),
    path.join(__dirname, 'portal', '**', '*.js.map'),
  ])
})
