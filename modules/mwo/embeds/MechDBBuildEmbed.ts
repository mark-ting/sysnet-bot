import { User } from 'discord.js'
import { Embed } from 'models/Embed'
import { Mech, Loadout, Item, EquipmentMap } from 'modules/mwo/interfaces/mechdb/MechDB'

export class MechDBBuildEmbed extends Embed {
  constructor (user: User, mechlabUrl: string, mech: Mech, loadout: Loadout, equipment: EquipmentMap) {
    super()

    this.setTitle(`${mech.Chassis} - ${mech.DisplayName}`)
    this.setDescription(`[Build](${mechlabUrl}) linked by ${user.toString()} for the ${mech.DisplayName} (${mech.Affiliation} ${mech.Tonnage}t).`)

    const weapons = {} as { [name: string]: number }
    const ammo = {} as { [name: string]: number }
    const equips = {} as { [name: string]: number }
    Object.keys(loadout.Slots).forEach(location => {
      const items = loadout.Slots[location]
      items.forEach(id => {
        const item = equipment[id]
        if (item.ItemType === 'WEP') {
          if (item.Name in weapons) {
            weapons[item.Name]++
          } else {
            weapons[item.Name] = 1
          }
        }

        if (item.ItemType === 'AMM') {
          if (item.Name in ammo) {
            ammo[item.Name]++
          } else {
            ammo[item.Name] = 1
          }
        }

        if (item.ItemType === 'EQP') {
          if (item.Name in equips) {
            equips[item.Name]++
          } else {
            equips[item.Name] = 1
          }
        }
      })
    })

    const weaponsText = Object.keys(weapons)
      .map(name => `${weapons[name]} × ${name}`)
      .join('\n')
    if (weaponsText) {
      this.addField('Weapons', weaponsText, true)
    }

    const ammoText = Object.keys(ammo)
      .map(name => `${ammo[name]} × ${name}`)
      .join('\n')
    if (ammoText) {
      this.addField('Ammo', ammoText, true)
    }

    const equipmentText = Object.keys(equips)
      .map(name => `${equips[name]} × ${name}`)
      .join('\n')
    if (equipmentText) {
      this.addField('Equipment', equipmentText, true)
    }

    const upgradesText = [
      `Armor: ${loadout.ArmorType.toUpperCase()}`,
      `Structure: ${loadout.StructType.toUpperCase()}`,
      `Heatsink: ${loadout.hsType.toUpperCase()}`,
      `Guidance: ${loadout.Guidance.toUpperCase()}`
    ].join('\n')
    this.addField('Upgrades', upgradesText || 'n/a', true)

    const armorText = [
      '**```json',
      `HD: ${loadout.Armor.HD}`,
      `RA: ${loadout.Armor.RA}`,
      `RT: ${loadout.Armor.RT.f} | ${loadout.Armor.RT.r}`,
      `CT: ${loadout.Armor.CT.f} | ${loadout.Armor.CT.r}`,
      `LT: ${loadout.Armor.LT.f} | ${loadout.Armor.LT.r}`,
      `LA: ${loadout.Armor.LA}`,
      `RL: ${loadout.Armor.RL}`,
      `LL: ${loadout.Armor.LL}`,
      '```**'
    ].join('\n')
    this.addField('Armor Distribution', armorText || 'n/a', true)

    this.setTimestamp(new Date())
    const disclaimer = '• Values shown without quirks/skills. Alpha embed.'
    this.setFooter(disclaimer)
  }
}
