import { User } from 'discord.js'
import { Embed } from 'models/Embed'
import { Pilot, Season } from '../interfaces/JarlsList'

function seasonToGeneralStatText (season: Season) {
  if (season.Rank === 0) {
    return (
      `**PILOT NOT ACTIVE**\n` +
      `Games: **${season.GamesPlayed}**`
    )
  }

  return (
    `Season: **${season.Season || 'Overview'}**\n` +
    `Rank: **${season.Rank}**\n` +
    `Percentile: **${season.Percentile}%**\n` +
    `Games: **${season.GamesPlayed}**`
  )
}

function seasonToMatchStatText (season: Season) {
  return (
    `W/L Ratio: **${season.WLRatio}**\n` +
    `K/D Ratio: **${season.KDRatio}**\n` +
    `Survival: **${season.SurvivalRate}%**\n` +
    `Avg. Score: **${season.AverageMatchScore}**\n` +
    `Adj. Score: **${season.AdjustedScore}**`
  )
}

function seasonToDistributionText (season: Season) {
  return (
    `Light: **${season.LightPercent}%**\n` +
    `Medium: **${season.MediumPercent}%**\n` +
    `Heavy: **${season.HeavyPercent}%**\n` +
    `Assault: **${season.AssaultPercent}%**\n`
  )
}

export class JarlsListEmbed extends Embed {
  constructor (user: User, pilot: Pilot, season: Season) {
    super()

    const pilotStr = pilot.unit ? `[${pilot.unit}] ${pilot.name}` : pilot.name

    const prefix = season.Season ? `Season ${season.Season} Data` : 'Overview'
    this.setTitle(`${prefix} for: ${pilotStr}`)
    this.setDescription(`[Profile](${pilot.url}) linked by ${user.toString()}.`)

    // Global ranking stats
    const globalStatText = seasonToGeneralStatText(season)
    this.addField('General Stats', globalStatText || 'n/a', true)

    // Match stats
    const matchStatText = seasonToMatchStatText(season)
    this.addField('Match Stats', matchStatText || 'n/a', true)

    // Class distribution stats
    const distText = seasonToDistributionText(season)
    this.addField('Class Usage', distText || 'n/a', true)

    this.setTimestamp(new Date())
    this.attachFile('res/isen.png')
    this.setFooter(`Powered by the Jarl's List API. Special thanks to Scuro.`, 'attachment://isen.png')
  }
}
