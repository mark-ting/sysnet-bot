import { User } from 'discord.js'
import { Embed } from 'models/Embed'
import * as Format from '../helpers/BuildFormatting'
import { Loadout } from '../interfaces/smurfy/Loadout'
import { Mech } from '../interfaces/smurfy/Mech'

export class SmurfyConciseBuildEmbed extends Embed {
  constructor (user: User, mechlabUrl: string, mech: Mech, loadout: Loadout) {
    super()
    this.setTitle(`${mech.translated_name} - ${mech.chassis_translated}`)
    this.setDescription(`[${mech.translated_short_name} Build](${mechlabUrl}) linked by ${user.toString()}`)

    const stats = loadout.stats

    const weaponsText = Format.itemsToListText(stats.armaments)
    if (weaponsText) {
      this.addField('Weapons', weaponsText, true)
    }

    const ammoText = Format.itemsToListText(stats.ammunition)
    if (ammoText) {
      this.addField('Ammo', ammoText, true)
    }

    const equipmentText = Format.itemsToListText(stats.equipment)
    this.addField('Equipment', equipmentText || 'n/a', true)

    const upgradesText = Format.upgradesToListText(loadout.upgrades)
    this.addField('Upgrades', upgradesText || 'n/a', true)

    this.setTimestamp(new Date())
    const createdAt = `Created: ${new Date(loadout.created_at).toLocaleString('en-US', { timeZone: 'UTC' }) + ' UTC'}`
    this.setFooter(createdAt)
  }
}
