import axios, { AxiosError } from 'axios'
import { Message } from 'discord.js'
import { Command } from 'models/Command'
import { JarlsListEmbed } from 'modules/mwo/embeds/JarlsListEmbed'
import { Overview, Pilot, Season } from '../interfaces/JarlsList'

class JarlsListCommand extends Command {
  constructor () {
    super({
      name: 'jarls-list',
      args: [
        {
          name: 'pilot-name',
          required: true,
          description: 'MWO Pilot name. Names can be surrounded in quotes like: "example user with spaces".'
        },
        {
          name: 'season',
          required: false,
          description: 'Season to request data for. Defaults to overall stats/overview.'
        }
      ],
      permsNeeded: [],
      shortDesc: `Incoming potato recipes.`,
      longDesc: `Load the Jarl's List profile for a specified pilot.`,
      action: async (message, core, args) => {
        const name = args['pilot-name']
        if (!name) {
          message.reply('Pilot name required.')
          if (message.deletable) {
            message.delete()
          }
          return
        }

        const seasonNum = args['season']
        if (seasonNum && isNaN(parseInt(seasonNum, 10))) {
          message.reply('Invalid season value.')
          if (message.deletable) {
            message.delete()
          }
          return
        }

        if (name.length > 32) {
          message.reply('Pilot name can be no longer than 32 characters.')
          if (message.deletable) {
            message.delete()
          }
          return
        }

        let overview = {} as Overview
        try {
          const overviewUrl = `https://leaderboard.isengrim.org/api/usernames/${name}`
          const res = await axios.get(overviewUrl)
          overview = res.data as Overview
        } catch (e) {
          const err = e as AxiosError
          const status = err.response.status

          let reply: Message
          switch (status) {
            case 404:
              reply = await message.reply(`Pilot '${name}' not found!`) as Message
              break

            case 400:
              reply = await message.reply(`Invalid pilot.`) as Message
              break

            default:
              core.logger.error(`Jarl's List lookup failed: ${err.response.statusText}`)
              reply = await message.reply(err.response.data.detail) as Message
              break
          }

          reply.delete(5000)
          if (message.deletable) {
            message.delete()
          }
          return
        }

        let season = overview as Season
        if (seasonNum) {
          try {
            const seasonUrl = `https://leaderboard.isengrim.org/api/usernames/${name}/seasons/${seasonNum}`
            const res = await axios.get(seasonUrl)
            season = res.data as Season
          } catch (e) {
            const err = e as AxiosError
            const reply = await message.reply(err.response.data.detail) as Message as Message
            reply.delete(5000)
            if (message.deletable) {
              message.delete()
            }
            return
          }
        }

        const pilotName = overview.PilotName
        const urlName = pilotName.split(' ').join('+')
        const pilotUrl = `https://leaderboard.isengrim.org/search?u=${urlName}`

        const pilot: Pilot = {
          name: pilotName,
          unit: overview.UnitTag,
          url: pilotUrl
        }

        const embed = new JarlsListEmbed(message.author, pilot, season)
        message.channel.send(embed)
        if (message.deletable) {
          message.delete()
        }
      }
    })
  }
}

export = JarlsListCommand
