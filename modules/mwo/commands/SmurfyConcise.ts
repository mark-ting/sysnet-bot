import axios from 'axios'
import { Command } from 'models/Command'
import { SmurfyConciseBuildEmbed } from '../embeds/SmurfyConciseBuildEmbed'
import { createLoadoutUrl, createMechUrl, matchSmurfyUrl } from '../helpers/SmurfyUtils'
import { Loadout } from '../interfaces/smurfy/Loadout'
import { Mech } from '../interfaces/smurfy/Mech'

class SmurfyConciseCommand extends Command {
  constructor () {
    super({
      name: 'smurfyc',
      args: [
        {
          name: 'mechlab-url',
          required: true,
          description: 'Smurfy mechlab build link.'
        }
      ],
      permsNeeded: [],
      shortDesc: `Incoming potato.`,
      longDesc: `Load a concise summary of a Smurfy build.`,
      action: async (message, core, args) => {
        const mechlabUrl = args['mechlab-url'] as string
        if (!mechlabUrl) {
          message.reply('You must provide a Smurfy URL!')
          return
        }

        const match = matchSmurfyUrl(mechlabUrl)
        if (!match) {
          message.reply('The Smurfy URL you provided was invalid!')
          return
        }

        const getDataUrl = axios.get(createLoadoutUrl(match))
        const getMechUrl = axios.get(createMechUrl(match))

        const [loadoutReq, mechReq] = await Promise.all([getDataUrl, getMechUrl])
        const loadout = loadoutReq.data as Loadout
        const mech = mechReq.data as Mech
        const embed = new SmurfyConciseBuildEmbed(message.author, mechlabUrl, mech, loadout)
        message.channel.send(embed)
        if (message.deletable) {
          message.delete()
        }
      }
    })
  }
}

export = SmurfyConciseCommand
