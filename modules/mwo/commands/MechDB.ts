import axios from 'axios'
import { Command } from 'models/Command'
import { MechDBBuildEmbed } from 'modules/mwo/embeds/MechDBBuildEmbed'
import * as XRegExp from 'xregexp'
import { EquipmentMap, Item, Loadout, Mech, LoadoutResponse } from '../interfaces/mechdb/MechDB'

interface MechDBExpExecArray extends RegExpExecArray {
  token: string
}

class MechDBCommand extends Command {
  constructor () {
    super({
      name: 'mechdb',
      args: [
        {
          name: 'mechlab-url',
          required: true,
          description: 'MechDB mechlab build link.'
        }
      ],
      permsNeeded: [],
      shortDesc: `Incoming potato.`,
      longDesc: `Load a detailed summary of a MechDB build. Very Alpha.`,
      action: async (message, core, args) => {
        const mechlabUrl = args['mechlab-url'] as string
        if (!mechlabUrl) {
          message.reply('You must provide a MechDB URL!')
          return
        }

        const urlExp = XRegExp(`^https?://(www.)?(mech.)?nav-alpha.com(/mech)?/#(?<token>[a-zA-Z0-9_-]+)`, 'n')
        const match = XRegExp.exec(mechlabUrl, urlExp) as MechDBExpExecArray
        if (!match) {
          message.reply('The MechDB URL you provided was invalid!')
          return
        }

        const getClanEquipment = axios.get(`https://www.nav-alpha.com/mech/php/mechlab_getEquipment.php?affiliation=Clan`)
        const getISEquipment = axios.get(`https://www.nav-alpha.com/mech/php/mechlab_getEquipment.php?affiliation=InnerSphere`)
        const [ISItems, ClanItems] = await Promise.all([getISEquipment, getClanEquipment])
        const itemList = (ISItems.data as Item[]).concat(ClanItems.data as Item[])
        const equipment: EquipmentMap = {}
        itemList.forEach((item) => {
          equipment[item.ID] = item
        })

        const getDataUrl = axios.get(`https://www.nav-alpha.com/mech/php/mechlab_getMech.php?mech=${match.token}&customLoadout=1`)
        const variant = match.token.split('_')[1]
        const getMechUrl = axios.get(`https://www.nav-alpha.com/mech/php/mechlab_getMech.php?mech=${variant}&customLoadout=0`)

        const [loadoutReq, mechReq] = await Promise.all([getDataUrl, getMechUrl])
        const response = loadoutReq.data[0] as LoadoutResponse
        const loadout = JSON.parse(response.jsonString) as Loadout
        const mech = mechReq.data[0] as Mech

        const mechdbUrl = `https://mech.nav-alpha.com/#${match.token}`
        const embed = new MechDBBuildEmbed(message.author, mechdbUrl, mech, loadout, equipment)

        message.channel.send(embed)
        if (message.deletable) {
          message.delete()
        }
      }
    })
  }
}

export = MechDBCommand
