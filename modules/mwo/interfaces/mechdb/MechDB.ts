// Response from GETing build yields a JSON payload
export interface LoadoutResponse {
  Token: string
  Variant: string
  jsonString: string
  DateCreated: string
}

export interface Loadout {
  Variant: string
  ArmorType: string
  StructType: string
  hsType: string
  Guidance: string
  EngineID: string
  SlotsTaken: number
  Tons: string
  Slots: {
    [location: string]: string[]
    RA: string[]
    RT: string[]
    RL: string[]
    HD: string[]
    CT: string[]
    LT: string[]
    LL: string[]
    LA: string[]
  }
  hsCountE: number
  Armor: {
    [location: string]: number | { f: number; r: number}
    RA: number
    RT: {
      f: number
      r: number
    }
    RL: number
    HD: number
    CT: {
      f: number
      r: number
    }
    LT: {
      f: number
      r: number
    }
    LL: number
    LA: number
  }
}

export interface Mech {
  Chassis: string
  Tonnage: string
  Affiliation: 'Clan' | 'InnerSphere'
  ID: string
  Name: string
  DisplayName: string
  minEngine: string
  maxEngine: string
  maxJJS: string
  AMS: string
  ECM: string
  MASC: string
  Omni: string
  Variant: string
  // Total refers to armor
  Total: string
}

export interface Item {
  ID: string
  Name: string
  Description: string
  Slots: string
  Tons: string
  ItemType: 'EQP' | 'WEP' | 'AMM'
}

export interface EquipmentMap {
  [id: string]: Item
}
