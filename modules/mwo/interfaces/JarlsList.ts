export interface Overview extends Season {
  PilotName: string
  UnitTag: string
  FirstSeason: number
  LastSeason: number
}

export interface Season {
  Season?: number
  Rank: number
  Percentile: number
  TotalWins: number
  TotalLosses: number
  TotalKills: number
  WLRatio: number
  TotalDeaths: number
  KDRatio: number
  SurvivalRate: number
  GamesPlayed: number
  AverageMatchScore: number
  AdjustedScore: number
  Progress: number
  LightPercent: number
  MediumPercent: number
  HeavyPercent: number
  AssaultPercent: number
}

export interface Pilot {
  name: string
  unit?: string
  url: string
}
