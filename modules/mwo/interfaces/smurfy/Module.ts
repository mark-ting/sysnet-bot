import { Item } from './Item'

export interface Module extends Item {
  id: string
  tons?: number
  slots?: number
  name: string
  price: Price
  translated_name: string
  translated_desc: string
}

export interface Price {
  id: number
  mc: number
  cb: number
}
