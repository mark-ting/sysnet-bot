import { ItemCount, Price } from './Item'
import { Quirk } from './Quirk'
import { WeaponType } from './Weapon'

export interface Omnipod {
  details: OmnipodDetails
  configuration: OmnipodConfig
  price: Price
}

export interface OmnipodDetails {
  chassis: string
  set: string
  component: string // todo: fixed components
  translatedName: string
}

export interface OmnipodConfig {
  name: string
  internalSlots: any[]
  fixedSlots: any[]
  quirks?: Quirk[]
  hardpoints: any[]
}

interface HardpointCount extends ItemCount {
  type: WeaponType
}
