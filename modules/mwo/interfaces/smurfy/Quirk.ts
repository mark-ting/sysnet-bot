export interface Quirk {
  name: string
  translated_name: string
  value: number
}
