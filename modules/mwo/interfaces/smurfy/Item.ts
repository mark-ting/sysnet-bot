export interface Item {
  id: string
  type: string
  name?: string
}

export interface ItemCount extends Item {
  count: number
}

export interface Price {
  id: number
  mc: number
  cb: number
}
