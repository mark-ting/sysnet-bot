import { Module } from './Module'

export interface Weapon extends Module {
  id: string
  type: WeaponType
  family: WeaponFamily
}

export type WeaponType = 'BALLISTIC' | 'BEAM' | 'MISSILE' | 'AMS'

export type WeaponFamily = 'BALLISTIC' | 'BEAM' | 'MISSILE' | 'UTILS'
