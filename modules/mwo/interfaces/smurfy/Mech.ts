import { Quirk } from './Quirk'

export interface Mech {
  id: string
  name: string
  faction: 'InnerSphere' | 'Clan'
  mech_type: 'light' | 'medium' | 'heavy' | 'assault'
  omni_mech: boolean
  family: string
  chassis_translated: string
  translated_name: string
  translated_short_name: string
  details: MechDetails
}

export interface MechDetails {
  type: '' | 'Hero' | 'Champion' | 'Special' | 'Sarah'
  tons: string
  top_speed: number
  jump_jets: number
  ecm: boolean
  masc: boolean
  tech_slots: number
  weapon_mod_slots: number
  consumable_slots: number
  engine_range: { min: number, max: number}
  hardpoints: {
    ams: number,
    beam: number,
    ballistic: number,
    // "missle" typo is intentional
    missle: number
  }
  tuning_config: any
  max_armor: number
  quirks: Quirk[]
}
