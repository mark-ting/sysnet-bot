import { Item } from './Item'

export interface Component {
  name: ComponentLocation
  armor: number
  actuators: Actuator[]
  omnipod?: number
  items: Item[]
}

export type ComponentLocation = 'head'
| 'left_arm'
| 'right_arm'
| 'left_torso'
| 'centre_torso'
| 'right_torso'
| 'left_torso_rear'
| 'centre_torso_rear'
| 'right_torso_rear'
| 'right_leg'
| 'left_leg'

export interface Actuator {
  id: string,
  enabled: boolean
}
