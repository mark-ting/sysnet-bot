import { Component } from './Component'
import { Item } from './Item'

// A Smurfy Loadout is the colloquial "Build".
// They are, for all intents and purposes, synonymous.
// The term 'Loadout' better matches the Smurfy API
export interface Loadout {
  id: string
  mech_id: number
  valid: boolean,
  configuration: Component[],
  upgrades: Upgrade[]
  stats: LoadoutStats,
  created_at: Date
}

export interface Upgrade extends Item {
  id: string,
  type: 'Armor' | 'Structure' | 'HeatSink' | 'Artemis'
  name: string
}

export interface LoadoutStats {
  used_armor: number
  used_jump_jets: number
  granted_jump_jets?: number
  firepower: number
  dps_max: number
  dps_sustained: number
  cooling_efficiency: number
  heatsinks: number
  top_speed: number
  top_speed_tweak?: number
  engine_name: string
  engine_rating: number
  engine_type: string
  armaments: ItemCount[]
  ammunition: ItemCount[]
  equipment: ItemCount[]
}

export interface ItemCount {
  id: string
  type: string
  name: string
  count: number
}
