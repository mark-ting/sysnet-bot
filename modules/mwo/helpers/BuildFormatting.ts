import { Component, ComponentLocation } from '../interfaces/smurfy/Component'
import { ItemCount } from '../interfaces/smurfy/Item'
import { LoadoutStats, Upgrade } from '../interfaces/smurfy/Loadout'
import { Mech } from '../interfaces/smurfy/Mech'
import { Quirk } from '../interfaces/smurfy/Quirk'

export function itemsToListText (items: ItemCount[]) {
  return items.map(item => `${item.count} × ${item.name}`).join('\n')
}

export function upgradesToListText (upgrades: Upgrade[]) {
  return upgrades.map(upgrade => `${upgrade.type}: ${upgrade.name}`).join('\n')
}

export function quirksToListText (quirks: Quirk[]) {
  return quirks.map(quirk => {
    // Negative vals already have "-"
    let prefix = (quirk.value > 0) ? '+' : ''

    let valueText = quirk.name.endsWith('multiplier')
      ? prefix + (100 * quirk.value).toString() + '%'
      : prefix + quirk.value.toString()

    return `${quirk.translated_name}: **${valueText}**`
  }).join('\n')
}

export function statsToListText (mech: Mech, stats: LoadoutStats) {
  const internalHS = Math.min(10, Math.floor(stats.engine_rating / 25) || 0)
  const externalHS = stats.heatsinks - internalHS

  return (
    '**```json'
    + `\n Firepower: ${stats.firepower.toFixed(2)}`
    + `\n   Cooling: ${stats.cooling_efficiency}%`
    + `\n Sust. DPS: ${stats.dps_sustained.toFixed(2)} DPS`
    + `\n  Max. DPS: ${stats.dps_max.toFixed(2)} DPS`
    + `\n     Armor: ${stats.used_armor} / ${mech.details.max_armor}`
    + `\n Jump Jets: ${stats.used_jump_jets} / ${mech.details.jump_jets}`
    + `\n Heatsinks: ${stats.heatsinks}`
    + `\n EX / (IN): ${externalHS} / (${internalHS})`
    + `\n Top Speed: ${stats.top_speed ? stats.top_speed.toFixed(1) : '0.0' } kph`
    + '```**'
  )
}

export function configToArmorListText (config: Component[]) {
  const armor: Map<ComponentLocation, number> = new Map()
  for (const component of config) { armor.set(component.name, component.armor || 0) }

  return (
    '**```json'
    + `\n HD: ${armor.get('head')}`
    + `\n RA: ${armor.get('right_arm')}`
    + `\n RT: ${armor.get('right_torso')} | ${armor.get('right_torso_rear')}`
    + `\n CT: ${armor.get('centre_torso')} | ${armor.get('centre_torso_rear')}`
    + `\n LT: ${armor.get('left_torso')} | ${armor.get('left_torso_rear')}`
    + `\n LA: ${armor.get('left_arm')}`
    + `\n RL: ${armor.get('right_leg')}`
    + `\n LL: ${armor.get('left_leg')}`
    + '```**'
  )
}

// TODO: clean up paper doll construction
export function configToPaperDoll (config: Component[]) {
  const armor: Map<ComponentLocation, number> = new Map()
  for (const component of config) { armor.set(component.name, component.armor || 0) }

  const hd = centerText(armor.get('head').toFixed(0), 3)
  const ra = centerText(armor.get('right_arm').toFixed(0), 2)
  const rt = centerText(armor.get('right_torso').toFixed(0), 4)
  const tr = centerText('(' + armor.get('right_torso_rear').toFixed(0) + ')', 4)
  const ct = centerText(armor.get('centre_torso').toFixed(0), 5, true)
  const tc = centerText('(' + armor.get('centre_torso_rear').toFixed(0) + ')', 5, true)
  const lt = centerText(armor.get('left_torso').toFixed(0), 4, true)
  const tl = centerText('(' + armor.get('left_torso_rear').toFixed(0) + ')', 4, true)
  const la = centerText(armor.get('left_arm').toFixed(0), 2, true)
  const rl = centerText(armor.get('right_leg').toFixed(0), 3)
  const ll = centerText(armor.get('left_leg').toFixed(0), 3, true)

  return (
    '**```json' +
    `\n R     ╔═══╗     L  ║ HD: ${armor.get('head')}` +
    `\n ┌─────╢${hd}╟─────┐  ║ RA: ${armor.get('right_arm')}` +
    `\n┌┤${rt}╔╩═══╩╗${lt}├┐ ║ RT: ${armor.get('right_torso')} | ${armor.get('right_torso_rear')}` +
    `\n││${tr}║${ct}║${tl}││ ║ CT: ${armor.get('centre_torso')} | ${armor.get('centre_torso_rear')}` +
    `\n│└─┬──╢${tc}╟──┬─┘│ ║ LT: ${armor.get('left_torso')} | ${armor.get('left_torso_rear')}` +
    `\n│${ra}└┐┌╚══╦══╝┐┌┘${la}│ ║ LA: ${armor.get('left_arm')}` +
    `\n└───┘│   ║   │└───┘ ║ RL: ${armor.get('right_leg')}` +
    `\n    ┌┘${rl}║${ll}└┐     ║ LL: ${armor.get('left_leg')}` +
    `\n    └────╨────┘     ║` +
    '```**'
  )
}

/**
 * Centers a string with spaces as closely as possible. Starts on the right side.
 * If string is of length or greater than length, returns original string.
 * @param text Text to pad.
 * @param targetLength Desired length.
 */
function centerText (text: string, targetLength: number, startLeft: boolean = false) {
  const length = text.length
  if (length >= targetLength) {
    return text
  }

  let steps = targetLength - length
  let left = startLeft
  while (steps > 0) {
    text = left ? ' ' + text : text + ' '

    // flip sides for next step
    left = !left
    steps--
  }

  return text
}
