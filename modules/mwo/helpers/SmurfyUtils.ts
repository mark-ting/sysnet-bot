import * as XRegExp from 'xregexp'

export interface SmurfyExpExecArray extends RegExpExecArray {
  mechId: string,
  buildHash: string
}

const smurfyUrlExp = XRegExp(`^https?://mwo.smurfy-net.de/mechlab#i=(?<mechId>[0-9]+)&l=(?<buildHash>[a-zA-Z0-9]+)`)

export function matchSmurfyUrl (mechlabUrl: string) {
  return XRegExp.exec(mechlabUrl, smurfyUrlExp) as SmurfyExpExecArray
}

export function createLoadoutUrl (match: SmurfyExpExecArray) {
  return `http://mwo.smurfy-net.de/api/data/mechs/${match.mechId}/loadouts/${match.buildHash}.json`
}

export function createMechUrl (match: SmurfyExpExecArray) {
  return `http://mwo.smurfy-net.de/api/data/mechs/${match.mechId}.json`
}
