export interface RewardType {
  name: string
  description: string
  test: string
  thumbnail: string
  color: string
}

export interface Reward {
  items: Array<string>
  countedItems: Array<Object>
  credits: number

  getTypes (): Array<string>
  getTypesFull (): Array<RewardType>
  toString (): string
}

export interface Mission {
  description: string | null
  node: string
  type: string
  faction: string
  reward?: Reward
  minEnemyLevel: number
  maxEnemyLevel: number
  maxWaveNum: number
  nightmare: boolean
  archwingRequired: boolean

  toString (): string
}

export interface Alert {
  activation: Date
  expiry: Date
  id: string
  mission: Mission

  getDescription (): string
  getETAString (): string
  getExpired (): boolean
  getReward (): Reward
  getRewardTypes (): Array<string>
  toString (): string
}
