import { Embed } from 'models/Embed'
import { Alert } from '../interfaces/WarframeWorldstate'

export class WarframeAlertEmbed extends Embed {
  constructor (alerts: Array<Alert>) {
    super()

    const est = Date.now() - (5 * 60 * 60 * 1000)
    const estTime = new Date(est)

    this.setTitle('Warframe Alerts')
    this.setDescription(`List of active alerts as of ${estTime.toLocaleString()}.`)
    for (let i = 0; i < alerts.length; i++) {
      const alert = alerts[i]

      const reward = alert.getReward().toString()
      const remaining = alert.getETAString()
      const alertTitle = `${reward}: ${remaining} remaining`

      const mission = alert.mission
      const alertDesc = `${mission.node} - ${mission.faction} ${mission.type}\n(Lvl. ${mission.minEnemyLevel} - ${mission.maxEnemyLevel})`

      this.addField(alertTitle, alertDesc)
    }
    this.setFooter('Uses `warframe-worldstate-parser`. Thanks to Warframe Community Developers!')
  }
}
