import { Command } from 'models/Command'
import { WarframeAlertEmbed } from '../embeds/WarframeAlertEmbed'
import { Alert } from '../interfaces/WarframeWorldstate'
const WorldState = require('warframe-worldstate-parser')

class WarframeAlertCommand extends Command {
  constructor () {
    super({
      name: 'wfalerts',
      args: [],
      permsNeeded: [],
      shortDesc: 'Quick! Nitain!',
      longDesc: 'Displays a list of currently active Warframe alerts.',
      action: async (message, core, args) => {
        const worldStateUrl = 'http://content.warframe.com/dynamic/worldState.php'
        const cache = core.store.requestCache(worldStateUrl)

        const data = await cache.fetch()
        const alerts: Array<Alert> = new WorldState(data).alerts
        const alertEmbed = new WarframeAlertEmbed(alerts)
        message.channel.send(alertEmbed)
      }
    })
  }
}

export = WarframeAlertCommand
