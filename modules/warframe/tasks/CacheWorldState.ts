import * as redis from 'redis'
import { Core } from 'src/Core'
import { Task } from 'models/Task'

const worldStateUrl = 'http://content.warframe.com/dynamic/worldState.php'

class CacheWarframeWorldStateTask extends Task {
  constructor () {
    super({
      name: 'CacheWarframeWorldState',
      description: 'Cache Warframe worldState for Alerts.',
      repeat: false,
      interval: 0,
      setup: (core: Core) => {
        const cache = core.store.requestCache(worldStateUrl, 10 * 1000)
      },
      action: (core: Core) => { return },
      cleanup: (core: Core) => {
        core.store.removeCache(worldStateUrl)
      }
    })
  }
}

export = CacheWarframeWorldStateTask
