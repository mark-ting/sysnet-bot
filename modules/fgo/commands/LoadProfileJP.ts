import { createLoadConfig } from 'modules/fgo/interfaces/LoadProfileConfig'
import { Command } from 'models/Command'

const region = 'jp'
const config = createLoadConfig(region)

class LoadProfileJPCommand extends Command {
  constructor () {
    super(config)
  }
}

export = LoadProfileJPCommand
