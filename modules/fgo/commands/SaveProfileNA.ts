import { createSaveConfig } from 'modules/fgo/interfaces/SaveProfileConfig'
import { Command } from 'models/Command'

const region = 'na'
const config = createSaveConfig(region)

class SaveProfileNACommand extends Command {
  constructor () {
    super(config)
  }
}

export = SaveProfileNACommand
