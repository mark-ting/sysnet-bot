import { createSaveConfig } from 'modules/fgo/interfaces/SaveProfileConfig'
import { Command } from 'models/Command'

const region = 'jp'
const config = createSaveConfig(region)

class SaveProfileJPCommand extends Command {
  constructor () {
    super(config)
  }
}

export = SaveProfileJPCommand
