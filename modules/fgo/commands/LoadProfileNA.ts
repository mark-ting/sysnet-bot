import { createLoadConfig } from 'modules/fgo/interfaces/LoadProfileConfig'
import { Command } from 'models/Command'

const region = 'na'
const config = createLoadConfig(region)

class LoadProfileNACommand extends Command {
  constructor () {
    super(config)
  }
}

export = LoadProfileNACommand
