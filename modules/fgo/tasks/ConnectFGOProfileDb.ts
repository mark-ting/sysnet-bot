import * as bluebird from 'bluebird'
import * as pg from 'pg-promise'
import { Core } from 'src/Core'
import { Task } from 'models/Task'

class ConnectFGOProfileDbTask extends Task {
  constructor () {
    super({
      name: 'ConnectFGOProfileDb',
      description: 'Connect to FGO Profile DB.',
      repeat: false,
      interval: 0,
      setup: (core: Core) => {
        const initOptions = {
          promiseLib: bluebird
        }

        const conn = {
          host: process.env.DB_HOST,
          port: +process.env.DB_PORT,
          database: process.env.DB_NAME,
          user: process.env.DB_USER,
          password: process.env.DB_PASS
        }

        // PG root
        const pgp: pg.IMain = pg(initOptions)
        const db: pg.IDatabase<{}> = pgp(conn)
        core.store.setDb('fgo', db)
        core.logger.info('F/GO Profile DB connection opened!')
      },
      action: (core: Core) => { return },
      cleanup: (core: Core) => {
        const db = core.store.getDb('fgo') as pg.IDatabase<{}>
        db.$pool.end()
        core.logger.info('F/GO Profile DB connection closed!')
      }
    })
  }
}

export = ConnectFGOProfileDbTask
