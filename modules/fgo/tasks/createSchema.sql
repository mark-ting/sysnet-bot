\connect sysnet
CREATE SCHEMA fgo;
ALTER SCHEMA fgo OWNER TO sysnet_bot;
SET default_with_oids = false;

-- JP Profiles
CREATE TABLE fgo.jp_profile (
    id          serial PRIMARY KEY,
    discord_id  numeric UNIQUE NOT NULL,
    user_id     character varying(12) NOT NULL,
    name        character varying(16),
    img_path    character varying(128) NOT NULL,
    updated     timestamp with time zone DEFAULT now() NOT NULL
);

ALTER TABLE fgo.jp_profile OWNER TO sysnet_bot;
ALTER TABLE fgo.jp_profile_id_seq OWNER TO sysnet_bot;
ALTER SEQUENCE fgo.jp_profile_id_seq OWNED BY fgo.jp_profile.id;

-- US Profiles
CREATE TABLE fgo.na_profile (
    id          serial PRIMARY KEY,
    discord_id  numeric UNIQUE NOT NULL,
    user_id     character varying(12) NOT NULL,
    name        character varying(16),
    img_path    character varying(128) NOT NULL,
    updated     timestamp with time zone DEFAULT now() NOT NULL
);

ALTER TABLE fgo.na_profile OWNER TO sysnet_bot;
ALTER TABLE fgo.na_profile_id_seq OWNER TO sysnet_bot;
ALTER SEQUENCE fgo.na_profile_id_seq OWNED BY fgo.na_profile.id;