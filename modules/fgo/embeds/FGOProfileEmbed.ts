import { User } from 'discord.js'
import { Embed } from 'models/Embed'
import { FGOProfile } from '../interfaces/FGOProfile'

export class FGOProfileEmbed extends Embed {
  constructor (region: string, user: User, profile: FGOProfile, requester: User) {
    super()
    this.setTitle(`F/GO ${region} Profile for ${user.username}`)
    this.setColor('cyan')
    this.attachFile(profile.imgPath)
    this.setThumbnail(user.avatarURL)
    this.setImage(`attachment://${profile.imgPath.split('/').pop()}`)
    this.addField('Friend Code', profile.userId, true)
    this.addField('Username', profile.name || '*n/a*', true)
    const dateStr = new Date(profile.updated).toLocaleString('en-US', { timeZone: 'UTC' }) + ' UTC'
    this.addField('Last Updated', dateStr, false)
    this.setTimestamp(new Date())
    this.setFooter(`Requested by ${requester.toString()}`)
  }
}
