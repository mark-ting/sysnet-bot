import * as fs from 'fs'
import { multiMkdirSync } from 'helpers/FSUtils'
import * as Jimp from 'jimp'
import { Command } from 'models/Command'
import { FGOProfile, profileFromSqlRow } from '../interfaces/FGOProfile'
import * as path from 'path'
import { IDatabase } from 'pg-promise'
import { Database } from 'src/Store'

const defaultImgPath = path.join('res', 'noimage.png')

export function createSaveConfig (region: string): Command.Config {
  const regionUpper = region.toUpperCase()
  return {
    name: `save-fgo${region}`,
    args: [
      {
        name: 'user-id',
        required: false,
        description: 'F/GO User ID (aka Friend Code) in the format: `###,###,###`'
      },
      {
        name: 'profile-name',
        required: false,
        description: 'F/GO profile username.'
      }
    ],
    permsNeeded: [],
    shortDesc: `Save ${regionUpper} F/GO Profile.`,
    longDesc: `Save ${regionUpper} F/GO Profile. Attach a support image to show your roster!`,
    action: async (message, core, args) => {
      const db: Database = core.store.getDb('fgo') as IDatabase<{}>
      const discordId = message.author.id

      let profile: FGOProfile = {
        userId: null,
        discordId: discordId,
        name: args['profile-name'],
        imgPath: defaultImgPath,
        updated: null
      }
      let update = false

      try {
        const values = {
          schema: 'fgo',
          table: `${region}_profile`,
          discordId: discordId
        }
        const loadQuery = 'SELECT * FROM ${schema:name}.${table:name} WHERE discord_id = ${discordId};'
        const data = await db.oneOrNone(loadQuery, values)
        if (data) {
          profile = Object.assign(profile, profileFromSqlRow(data))
          update = true
        }
      } catch (err) {
        core.logger.error(err.message)
        message.reply('Profile database error. Please try again later.')
      }

      // Require user ID if no previous profile found
      const userId = args['user-id']
      if (userId) {
        profile.userId = userId
      } else if (!update) {
        message.reply('You must provide a User ID! Please use `help` for more info.')
        return
      }

      // Check attachment exists
      if (message.attachments.first()) {
        const attachment = message.attachments.first()
        const attachUrl = attachment.url

        const img = await Jimp.read(attachUrl)
        img.scaleToFit(960, 540, Jimp.AUTO).quality(75)

        const savedDir = path.join('res', 'saved', region)
        if (!fs.existsSync(savedDir)) { multiMkdirSync(savedDir, true) }

        // Workaround for Jimp.write not working
        img.getBuffer(Jimp.MIME_JPEG, (err: Error, imgBuffer: Buffer) => {
          if (err) {
            core.logger.error(err.message)
            return
          }
          const fileName = discordId + '.jpeg'

          // Update profile image path
          profile.imgPath = path.join(savedDir, fileName)
          fs.createWriteStream(profile.imgPath).write(imgBuffer)
        })
      }

      try {
        const values = Object.assign(profile, {
          schema: 'fgo',
          table: `${region}_profile`
        })
        console.log(values)
        const updateQuery = 'UPDATE ${schema:name}.${table:name} SET (user_id, name, img_path) = (${userId}, ${name}, ${imgPath}) WHERE discord_id = ${discordId} RETURNING *;'
        const insertQuery = 'INSERT INTO ${schema:name}.${table:name} (discord_id, user_id, name, img_path) VALUES (${discordId}, ${userId}, ${name}, ${imgPath}) RETURNING *;'
        const query = update ? updateQuery : insertQuery
        await db.one(query, values)
        message.reply(`${regionUpper} profile successfully ${update ? 'updated' : 'saved'}!`)
      } catch (err) {
        core.logger.error(err.message)
        message.reply('Unable to save profile. Please try again later.')
      }

      if (message.deletable) {
        message.delete()
      }
    }
  }
}
