export interface FGOProfile {
  userId: string,
  discordId: string,
  name?: string,
  imgPath: string,
  updated: number
}

/**
 * Convert Object returned from pg-promise to FGOProfile
 */
export function profileFromSqlRow (data: any): FGOProfile {
  return {
    userId: data['user_id'],
    discordId: data['discord_id'],
    name: data['name'],
    imgPath: data['img_path'],
    updated: data['updated']
  }
}
