import { DiscordAPIError, Message } from 'discord.js'
import { Command } from 'models/Command'
import { IDatabase } from 'pg-promise'
import { Database } from 'src/Store'
import { FGOProfileEmbed } from '../embeds/FGOProfileEmbed'
import { profileFromSqlRow } from './FGOProfile'

export function createLoadConfig (region: string): Command.Config {
  const regionUpper = region.toUpperCase()
  return {
    name: `load-fgo${region}`,
    args: [
      {
        name: 'discord-id',
        required: false,
        description: 'Discord user @mention or Snowflake ID.'
      }
    ],
    permsNeeded: [],
    shortDesc: `Load ${regionUpper} F/GO Profile.`,
    longDesc: `Load ${regionUpper} F/GO Profile. Hey, comes in a nice pretty embed too!`,
    action: async (message, core, args) => {
      let user = message.author
      let discordId = message.author.id

      if (args['discord-id']) {
        const match: string[] | null = args['discord-id'].match(/\d+/)
        if (match) {
          discordId = match[0]
          try {
            user = await core.client.fetchUser(discordId)
          } catch (err) {
            const error: DiscordAPIError = err
            // Discord Error 10013: 'Unknown User'
            if (err.code === 10013) {
              message.reply('User not found. Please @mention them or check their Snowflake ID.')
              return
            }
          }
        }
      }

      const db: Database = core.store.getDb('fgo') as IDatabase<{}>
      try {
        const values = {
          schema: 'fgo',
          table: `${region}_profile`,
          discordId: discordId
        }
        const loadQuery = 'SELECT * FROM ${schema:name}.${table:name} WHERE discord_id = ${discordId}'
        const data = await db.oneOrNone(loadQuery, values)
        if (!data) {
          const reply = await message.reply(`${regionUpper} profile not found for ${user.username}!`) as Message
          if (reply.deletable) { reply.delete(10000) }
          return
        }

        const profile = profileFromSqlRow(data)
        const profileEmbed = new FGOProfileEmbed(regionUpper, user, profile, message.author)
        message.channel.send(profileEmbed)
      } catch (err) {
        core.logger.error(err.message)
        message.reply('Unable to load requested profile. Please try again later.')
      }

      if (message.deletable) {
        message.delete()
      }
    }
  }
}
