import { Message } from 'discord.js'
import { Command } from 'models/Command'

class HelloCommand extends Command {
  constructor () {
    super({
      name: 'hello',
      args: [
        {
          name: 'name',
          required: false,
          description: 'Somebody else...?'
        }
      ],
      permsNeeded: [],
      shortDesc: 'Hello~',
      longDesc: 'Replies to the user who initiated the command.',
      action: (message, core, args) => {
        let text = `is it me you're looking for~?`
        if (args['name']) {
          text += ` ...or were you looking for "${args['name']}"?`
        }

        message.reply(text)
          .then((reply: Message) => {
            if (reply.deletable) {
              reply.delete(5000)
            }
          })
      }
    })
  }
}

export = HelloCommand
