import { Command } from 'models/Command'
import { HelpEmbed } from '../embeds/HelpEmbed'
import { UsageEmbed } from '../embeds/UsageEmbed'

class HelpCommand extends Command {
  constructor () {
    super({
      name: 'help',
      args: [
        {
          name: 'command',
          required: false,
          description: 'Name of command.'
        }
      ],
      permsNeeded: [],
      shortDesc: 'Lists available commands.',
      longDesc: 'Lists available commands to the user in a private message.',
      action: (message, core, args) => {
        // if (message.channel.type !== 'dm') {
        //   message.reply('please check your inbox!')
        //     .then((reply: Message) => {
        //       if (reply.deletable) {
        //         reply.delete(5000)
        //       }
        //     })
        // }

        let embed
        if (args['command']) {
          if (core.commandList.has(args['command'])) {
            const command = core.commandList.get(args['command'])
            embed = new UsageEmbed(command)
          } else {
            message.reply(`\`${args['command']}\` is not a recognized command!`)
          }
        } else {
          embed = new HelpEmbed(core.commandList)
        }
        message.channel.send(embed)
      }
    })
  }
}

export = HelpCommand
