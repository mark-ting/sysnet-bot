import { Command } from 'models/Command'

class RollDiceCommand extends Command {
  constructor () {
    super({
      name: 'roll',
      args: [
        {
          name: 'count',
          required: false,
          description: 'Number of dice to roll.'
        },
        {
          name: 'faces',
          required: false,
          description: 'Number of faces on each die.'
        }
      ],
      permsNeeded: [],
      shortDesc: 'No loaded dice here, promise! ;)',
      longDesc: 'Rolls a number of dice with specified number of sides. If no sides are specified, assumes 1d6.',
      action: (message, core, args) => {
        const count = parseInt(args['count'], 10) || 1
        const faces = parseInt(args['faces'], 10) || 6

        if (count < 1 || faces < 1) {
          message.reply(`Can't roll negatives!`)
        }

        const rolls = []

        for (let i = 0; i < count; i++) {
          rolls.push(Math.floor(Math.random() * faces + 1))
        }
        message.reply(`Rolled **\`${count}d${faces}\`** for a total of **\`${rolls.reduce((a, b) => (a + b))}\`**`)
      }
    })
  }
}

export = RollDiceCommand
