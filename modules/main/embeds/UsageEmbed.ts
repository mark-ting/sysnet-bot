import { Command } from 'models/Command'
import { Embed } from 'models/Embed'

function prettyArgName (arg: Command.Argument) {
  return arg.required ? `<${arg.name}>` : `[${arg.name}]`
}

function prettyArgDesc (arg: Command.Argument) {
  return arg.name + (arg.required ? ' <required>' : ' [optional]') + '\n\t' + arg.description + '\n'
}

export class UsageEmbed extends Embed {
  constructor (command: Command) {
    super()

    this.setTitle(`About: \`${command.name}\``)
    this.setDescription(command.longDesc)

    const args = command.args
    let usageArr = [command.name]
    for (let i = 0; i < args.length; i++) {
      const arg = args[i]
      usageArr.push(prettyArgName(arg))
    }
    const usage = '`' + usageArr.join(' ') + '`'
    this.addField('Usage', usage)

    let argText = ''
    args.forEach((arg) => {
      argText += prettyArgDesc(arg)
    })

    if (!argText) {
      argText = 'No arguments.'
    }

    this.addField('Arguments', argText)
  }
}
