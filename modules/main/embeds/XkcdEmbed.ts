import { Embed } from 'models/Embed'

interface XkcdData {
  month: string,
  num: string,
  link: string,
  year: string,
  news: string,
  safe_title: string,
  transcript: string,
  alt: string,
  img: string,
  title: string,
  day: number
}

export class XkcdEmbed extends Embed {
  constructor (data: XkcdData) {
    super()

    let pubDate = `${data.year}-${data.month}-${data.day}`
    let srcUrl = `https://xkcd.com/${data.num}`

    this.setTitle(data.title)
    this.setDescription(`[xkcd #${data.num} published on ${pubDate}](${srcUrl})`)
    this.addField('Alt text', data.alt)
    this.setImage(data.img)
  }
}
