import { Command } from 'models/Command'
import { Embed } from 'models/Embed'

export class HelpEmbed extends Embed {
  constructor (commandList: Command.List, commandName?: string) {
    super()

    this.setTitle('Available Commands')
    this.setDescription('List of valid bot commands.')
    commandList.forEach((command, commandName) => {
      let shortDesc = command.shortDesc
      this.addField(`\`${commandName}\``, shortDesc)
    })
  }
}
