require('module-alias/register')
require('dotenv').config()

if (!process.env.ENV_CONFIG_SET) {
  console.log('Please confgure the .env file!')
  process.exit()
}

import { Core } from 'src/Core'
import { Logger } from 'src/Logger'
import { Client } from 'discord.js'

const logger = new Logger(process.env.LOG_TO_CONSOLE === '1')
const client = new Client()
const prefix = process.env.BOT_PREFIX
const token = process.env.BOT_TOKEN
const bot = new Core(logger, client, token, prefix)
bot.init()

// Handle SIGINT on Windows arch
if (process.platform === 'win32') {
  let rl = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
  })

  rl.on('SIGINT', () => {
    process.emit('SIGINT')
  })
}

// Gracefully exit
process.on('SIGINT', () => {
  bot.kill()
  logger.info('Bot exiting.')
  process.exit()
})
