import { Cache } from 'models/Cache'
import { Component } from 'models/Component'
import { IDatabase } from 'pg-promise'
import { RedisClient } from 'redis'
import { Core } from 'src/Core'

// Defines valid Database accessor objects
export type Database = RedisClient | IDatabase<{}>

export type CacheStore = Map<string, Cache>
export type DbStore = Map<string, Database>

export class Store extends Component {
  private cacheStore: CacheStore
  private dbStore: DbStore

  constructor (core: Core) {
    super(core)
    this.cacheStore = new Map()
    this.dbStore = new Map()
  }

  public setDb (name: string, db: Database) {
    this.dbStore.set(name, db)
  }

  public getDb (name: string) {
    return this.dbStore.get(name)
  }

  public removeCache (url: string) {
    this.cacheStore.delete(url)
  }
  public requestCache (url: string, timeout?: number) {
    if (!this.cacheStore.has(url)) {
      this.addCache(url, timeout)
      this.logger.info(`Cache miss: '${url}'`)
    } else {
      return this.cacheStore.get(url)
    }
  }
  private addCache (url: string, timeout?: number) {
    try {
      let cache = new Cache(this.logger, url, timeout || 10 * 1000)
      this.cacheStore.set(url, cache)
      return cache
    } catch (err) {
      this.logger.error(err)
    }
  }
}
