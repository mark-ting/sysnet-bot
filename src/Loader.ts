import * as fs from 'fs'
import { Command } from 'models/Command'
import { Component } from 'models/Component'
import { Module } from 'models/Module'
import { Task } from 'models/Task'
import * as path from 'path'
import { Core } from 'src/Core'

export class Loader extends Component {
  constructor (core: Core) {
    super(core)
  }

  public loadModules () {
    const modules = this.enumModules()
    modules.forEach((mod) => {
      this.core.moduleList.set(mod.name, mod)
      this.loadModuleCommands(mod)
      this.loadModuleTasks(mod)
    })
  }

  private enumModules () {
    const moduleRoot = path.resolve('modules')
    const moduleHits = fs.readdirSync(moduleRoot)
    const modules: Module[] = []
    moduleHits.forEach((moduleName) => {
      const modulePath = path.join(moduleRoot, moduleName)
      if (fs.statSync(modulePath).isDirectory()) {
        modules.push({
          name: moduleName,
          path: modulePath,
          commands: new Map(),
          tasks: new Map()
        })
      }
    })
    this.logger.info(`${modules.length} module(s) found.`)
    return modules
  }

  private loadModuleCommands (mod: Module) {
    const cmdDir = path.join(mod.path, 'commands')
    if (!fs.existsSync(cmdDir)) {
      this.logger.info(`No commands for module '${mod.name}'`)
      return
    }
    const files = fs.readdirSync(cmdDir)
    files.forEach((file) => {
      if (path.extname(file) !== '.js') { return }

      const filePath = path.join(cmdDir, file)
      const cmdModule = require(filePath)
      if (cmdModule instanceof Function === false) {
        this.logger.warn(`'${file}' is not a valid command file!`)
        return
      }

      const command: Command = new cmdModule()
      if (command instanceof Command === false) {
        this.logger.warn(`Failed to initialize command from '${file}'!`)
        return
      }

      if (this.core.commandList.has(command.name)) {
        this.logger.warn(`Duplicate command '${command.name}' detected. Some commands may not work until resolved.`)
      } else {
        mod.commands.set(command.name, command)
        this.core.commandList.set(command.name, command)
        this.logger.info(`Loaded command '${command.name}'`)
      }
    })
  }

  private loadModuleTasks (mod: Module) {
    const taskDir = path.join(mod.path, 'tasks')
    if (!fs.existsSync(taskDir)) {
      this.logger.info(`No tasks for module '${mod.name}'`)
      return
    }

    const files = fs.readdirSync(taskDir)
    files.forEach((file) => {
      if (path.extname(file) !== '.js') {
        return
      }

      const filePath = path.join(taskDir, file)
      const taskModule = require(filePath)
      if (taskModule instanceof Function === false) {
        this.logger.warn(`'${file}' is not a valid task file!`)
        return
      }

      const task: Task = new taskModule()
      if (task instanceof Task === false) {
        this.logger.warn(`Failed to initialize task from '${file}'!`)
        return
      }

      if (this.core.taskList.has(task.name)) {
        this.logger.warn(`Duplicate task '${task.name}' detected. Some tasks may not run as expected until resolved.`)
      } else {
        mod.tasks.set(task.name, task)
        this.core.taskList.set(task.name, task)
        this.logger.info(`Loaded task '${task.name}'`)
      }
    })
  }
}
