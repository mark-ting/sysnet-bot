import * as express from 'express'
import { Command } from 'models/Command'
import { Component } from 'models/Component'
import { Task } from 'models/Task'
import * as path from 'path'
import { index } from 'portal/routes/index'
import { Core } from 'src/Core'

export class Portal extends Component {
  private commandList: Command.List
  private taskList: Task.List

  constructor (core: Core) {
    super(core)

    const port = process.env.PORT || 8080
    const app = express()
    app.set('view engine', 'pug')

    const viewDir = path.resolve('portal', 'views')
    app.set('views', viewDir)
    app.locals.basedir = viewDir
    app.use('/res', express.static('res'))
    app.use('/css', express.static('node_modules/spectre.css/dist'))

    app.use('/', index(core))

    app.listen(port, () => {
      this.logger.info(`Control panel listening on port ${port}.`)
    })
  }
}
