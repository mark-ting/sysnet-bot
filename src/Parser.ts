import { Message } from 'discord.js'
import { Component } from 'models/Component'
import { Core } from 'src/Core'
import * as xregexp from 'xregexp'

export interface MessageTokens {
  command: string
  args: string[]
}

export class Parser extends Component {
  constructor (core: Core) {
    super(core)
  }

  public parseMessage (message: Message): MessageTokens {
    const content = message.content
    // args separated by whitespaces and contained by quote chars
    const argExp = /("[^"\\]*(?:\\[\S\s][^"\\]*)*"|'[^'\\]*(?:\\[\S\s][^'\\]*)*'|\/[^\/\\]*(?:\\[\S\s][^\/\\]*)*\/[gimy]*(?=\s|$)|(?:\\\s|\S)+)/g
    const stripped = message.content.slice(this.core.prefix.length, content.length)
    const tokens = xregexp.match(stripped, argExp)

    const args = tokens.map((token) => {
      const prune = (
        (token.startsWith(`'`) && token.endsWith(`'`)) ||
        (token.startsWith(`"`) && token.endsWith(`"`))
      )

      return prune ? token.slice(1, token.length - 1) : token
    })

    return {
      command: args.shift(),
      args: args
    }
  }
}
