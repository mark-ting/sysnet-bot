import * as fs from 'fs'
import { multiMkdirSync } from 'helpers/FSUtils'
import * as moment from 'moment'
import * as path from 'path'

export class Logger {
  private debug: boolean
  private initTime: moment.Moment
  private logDir: string
  private logPath: string

  constructor (debug: boolean = false) {
    this.debug = debug
    this.initTime = moment()
    this.logDir = path.resolve('log')
    const logName = this.initTime.format('YYYY-MM-DD_HHmmss,SSS[.log]')
    this.logPath = path.join(this.logDir, logName)

    if (!fs.existsSync(this.logDir)) { multiMkdirSync(this.logDir) }

    try {
      fs.closeSync(fs.openSync(this.logPath, 'w'))
    } catch (err) {
      console.error(`Unable to create logfile. ${err}`)
    }
  }

  public error (message: string) {
    this.log('[ERROR]', message)
  }

  public warn (message: string) {
    this.log('[WARNING]', message)
  }

  public info (message: string) {
    this.log('[INFO]', message)
  }

  private timestamp (): string {
    const now = moment()
    return moment().format('YYYY-MM-DD HH:mm:ss,SSSS')
  }

  private log (tag: string, message: string) {
    if (this.debug) { console.log(message) }

    const line = `${this.timestamp()}\t${tag}\t${message}\r\n`
    try {
      fs.appendFileSync(this.logPath, line)
    } catch (err) {
      console.error(err)
    }
  }
}
