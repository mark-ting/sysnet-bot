import { Core } from 'src/Core'
import { Logger } from 'src/Logger'

export class Component {
  protected readonly logger: Logger

  constructor (protected readonly core: Core) {
    this.logger = core.logger
  }
}
