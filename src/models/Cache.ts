import * as http from 'http'
import * as https from 'https'
import { Logger } from 'src/Logger'

export class Cache {
  private logger: Logger
  private url: string
  private ssl: boolean
  private timeout: number
  private data: string
  private lastUpdated: number
  private lock: Promise<string>

  constructor (logger: Logger, url: string, timeout: number) {
    this.logger = logger
    this.url = url
    this.ssl = url.startsWith('https')
    this.timeout = timeout
    this.update()
  }

  /**
   * Returns stored cache data, updating if expired.
   * @returns {Promise<string>} Promise of response body.
   */
  public fetch (): Promise<string> {
    if (Date.now() - this.lastUpdated > this.timeout) {
      this.update()
    }
    return this.lock ? this.lock : Promise.resolve(this.data)
  }

  /**
   * Updates the cache data, locking with a Promise<string>.
   */
  private async update () {
    this.lock = this.httpGet()
    this.data = await this.lock
    this.lastUpdated = Date.now()
    this.lock = null
  }

  /**
   * Asynchronously performs a GET request to the cache URL.
   * @returns {Promise<string>} Promise of response body.
   */
  private httpGet (): Promise<string> {
    let get = this.ssl ? https.get : http.get

    return new Promise((resolve, reject) => {
      try {
        get(this.url, async (res) => {
          if (res.statusCode < 200 || res.statusCode > 299) {
            reject(new Error(`HTTP Error: ${res.statusCode}`))
          } else {
            let body: Array<string | Buffer> = []

            res.on('data', (chunk) => {
              body.push(chunk)
            })

            res.on('end', () => {
              resolve(body.join(''))
            })
          }
        })
      } catch (err) {
        reject(err)
      }
    })
  }
}
