import { Command } from 'models/Command'
import { Task } from 'models/Task'

export interface Module {
  name: string,
  path: string,
  commands: Command.List
  tasks: Task.List
}

export namespace Module {
  export type List = Map<string, Module>
}

