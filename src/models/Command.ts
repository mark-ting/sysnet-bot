import { Message, PermissionString } from 'discord.js'
import { Core } from 'src/Core'

export class Command implements Command.Config {
  public name: string
  public args: Command.Argument[]
  public permsNeeded: PermissionString[]
  public shortDesc: string
  public longDesc: string
  public action: Command.Action

  constructor (cfg: Command.Config) {
    Object.assign(this, cfg)
  }
}

export namespace Command {
  /**
   * Object representation of a Command
   */
  export interface Config {
    name: string
    args: Argument[]
    permsNeeded: PermissionString[]
    shortDesc: string
    longDesc: string
    action: Action
  }

  export interface Argument {
    name: string
    required?: boolean
    description?: string
  }

  export interface ArgValues {
    [argName: string]: any
  }

  export interface Action {
    (message: Message, core: Core, args?: ArgValues): void
  }

  export type List = Map<string, Command>
}
