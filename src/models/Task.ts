import { Core } from 'src/Core'

export class Task implements Task.Config {
  public name: string
  public description: string
  public repeat: boolean
  public interval: number
  public setup: Task.Function
  public action: Task.Function
  public cleanup: Task.Function

  constructor (cfg: Task.Config) {
    Object.assign(this, cfg)
  }
}

export namespace Task {
  export interface Config {
    name: string
    description: string
    repeat: boolean
    interval: number
    setup: Function
    action: Function
    cleanup: Function
  }

  export interface Function {
    (core: Core): void
  }

  export type List = Map<string, Task>
}
