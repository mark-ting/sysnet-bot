import { RichEmbed, RichEmbedOptions } from 'discord.js'

export class Embed extends RichEmbed {
  constructor (options?: RichEmbedOptions) {
    super(options)
  }
}
