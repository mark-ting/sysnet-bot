import { Message } from 'discord.js'
import { Command } from 'models/Command'
import { Component } from 'models/Component'
import { Core } from 'src/Core'
import { MessageTokens } from 'src/Parser'

export class Handler extends Component {
  constructor (core: Core) {
    super(core)
  }

  public scheduleTasks () {
    this.core.taskList.forEach((task) => {
      // Run setup and then schedule task loop
      task.setup(this.core)
      this.logger.info(`Task '${task.name}' setup complete.`)

      if (!task.repeat) { return }
      setInterval(task.action, task.interval, this.core)
      this.logger.info(`Task '${task.name}' scheduled to run every ${task.interval} ms.`)
    })
  }

  public terminateTasks () {
    this.core.taskList.forEach((task) => {
      task.cleanup(this.core)
      this.logger.info(`Task '${task.name}' cleaned up.`)
    })
  }

  /**
   * Check if parsed message is a valid command, and if so, performs necessary actions.
   * Non-command messages are ignored or logged as needed.
   * @param {Message} message
   */
  public handlePossibleCommand (message: Message, tokens: MessageTokens): void {
    const caller = message.member
    const commandName = tokens.command

    if (this.core.commandList.has(commandName)) {
      const command = this.core.commandList.get(commandName)
      if (!caller.hasPermission(command.permsNeeded)) {
        this.logger.info(`Permissions error: ${caller.id} attempted to call ${message.author.id}`)
        return
      }

      this.logger.info(`Valid command received.`)
      this.reactValid(message)

      const commandArgs = command.args
      const args: Command.ArgValues = {}
      for (let i = 0; i < commandArgs.length; i++) {
        const arg = commandArgs[i]
        args[arg.name] = tokens.args[i]
      }

      this.run(command, message, args)
    } else {
      this.logger.info(`Invalid command received.`)
      this.reactInvalid(message)
      message.reply(`\`${commandName}\` is not a valid command!`)
        .then((reply: Message) => {
          if (reply.deletable) {
            reply.delete(5000)
          }
        })
    }
  }

  /**
   * React to valid command message with a checkmark.
   * @param {Message} message - Command message.
   */
  private reactValid (message: Message) {
    message.react(String.fromCodePoint(0x2705))
  }

  /**
   * React to invalid command message with a cross.
   * @param {Message} message - Command message.
   */
  private reactInvalid (message: Message) {
    message.react(String.fromCodePoint(0x274c))
  }

  /**
   * Run a command action from a message with applicable arguments.
   * @param {Command} command - Command object.
   * @param {Message} message - Command message.
   * @param {CommandArgs} args - Arguments to pass to command action.
   */
  private run (command: Command, message: Message, args: Command.ArgValues) {
    command.action(message, this.core, args)
  }
}
