import * as Discord from 'discord.js'
import { Command } from 'models/Command'
import { Module } from 'models/Module'
import { Task } from 'models/Task'
import { Handler } from 'src/Handler'
import { Loader } from 'src/Loader'
import { Logger } from 'src/Logger'
import { Parser } from 'src/Parser'
import { Portal } from 'src/Portal'
import { Store } from 'src/Store'

export class Core {
  public readonly selfId: Discord.Snowflake

  // Commands and Tasks
  public moduleList: Module.List
  public commandList: Command.List
  public taskList: Task.List

  // Shared components
  public handler: Handler
  public parser: Parser
  public store: Store

  // Independent components
  private loader: Loader
  private portal: Portal

  // Command Prefix
  private commandPrefix: string

  constructor (public logger: Logger, public client: Discord.Client, private token: string, commandPrefix?: string) {
    this.prefix = commandPrefix
    this.moduleList = new Map()
    this.commandList = new Map()
    this.taskList = new Map()
    this.loader = new Loader(this)
    this.store = new Store(this)
    this.parser = new Parser(this)
    this.handler = new Handler(this)
  }

  public set prefix (prefix: string) {
    this.commandPrefix = prefix || '!'
    this.logger.info(`Command prefix set to: ${this.commandPrefix}`)
  }

  public get prefix () { return this.commandPrefix }

  public async init () {
    try {
      if (process.env.OFFLINE === '1') {
        this.logger.info('Started in offline mode.')
      } else {
        this.client.on('ready', () => {
          this.logger.info('Discord client connection successful.')
        })

        this.client.on('message', (message: Discord.Message) => {
          if (message.author.id === this.selfId) { return }
          if (!message.content.startsWith(this.commandPrefix)) { return }

          const tokens = this.parser.parseMessage(message)
          this.handler.handlePossibleCommand(message, tokens)
        })
        await this.client.login(this.token)
      }
    } catch (err) {
      this.logger.error(`Discord authentication error. ${err}`)
      await this.client.destroy()
      process.emit('SIGINT')
    }

    this.loader.loadModules()
    this.handler.scheduleTasks()
    this.portal = new Portal(this)
  }

  public kill () {
    if (!process.env.OFFLINE) {
      this.client.destroy()
    }
    this.handler.terminateTasks()
  }
}
