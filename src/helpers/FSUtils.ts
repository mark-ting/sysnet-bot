import * as fs from 'fs'
import * as path from 'path'

/**
 * @param {string} dir
 * @param {boolean} [useCwd=false] If dir is relative to Node working dir or calling script.
 */
export function multiMkdirSync (dir: string, useCwd: boolean = false) {
  const sep = path.sep
  const initDir = path.isAbsolute(dir) ? sep : ''
  const baseDir = useCwd ? '.' : __dirname

  dir.split(sep).reduce((parentDir: string, childDir: string) => {
    const curDir = path.resolve(baseDir, parentDir, childDir)

    if (!fs.existsSync(curDir)) {
      fs.mkdirSync(curDir)
    }
    return curDir
  }, initDir)
}
