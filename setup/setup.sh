DBUSER='sysnet_bot'

if sudo -u postgres psql -t -c '\du' | cut -d \| -f 1 | grep -qw $DBUSER; then
    echo "Role '$DBUSER' already exists!"
else
    DBPASS=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
    sudo -u postgres psql "CREATE ROLE \"$DBUSER\" WITH LOGIN ENCRYPTED PASSWORD '$DBPASS';"

    echo "Bot role set up with password:"
    echo "\n\"$DBPASS\"\n"
    echo "Please copy this password to your .env file or ALTER the user with your own!"
fi


DBNAME='sysnet'

if sudo -u postgres psql -lqt | cut -d \| -f 1 | grep -qw $DBNAME; then
    echo "DB '$DBNAME' already exists!"
else
    sudo -u postgres psql -f ./setup.sql
    echo "Bot DB setup!"
fi